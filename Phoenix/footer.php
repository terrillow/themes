		<div class="wrap">
            <footer id="bottom">
                <center>
					<p>Follow <?php echo site_name(); ?></p>
                	<?php if(twitter_account()) { ?>
               			<a href="<?php echo twitter_url(); ?>" target="_blank"<i class="fa fa-twitter icon"></i></a>
                	<?php } ?>	               			
                		<a href="https://github.com/terrillo" target="_blank"<i class="fa fa-github icon"></i></a>
                		<a href="https://plus.google.com/+TerrilloWalls" target="_blank"<i class="fa fa-google-plus icon"></i></a>
                		<a href="<?php echo rss_url(); ?>" target="_blank"<i class="fa fa-rss icon"></i></a>
                </center>
            </footer>

	        </div>
        </div>
        <div class="space"></div>
        
     </body>
</html>