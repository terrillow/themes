<?php theme_include('header');
	function getUrl() {
	  $url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
	  $url .= ( $_SERVER["SERVER_PORT"] !== 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
	  $url .= $_SERVER["REQUEST_URI"];
	  return $url;
	}
?>

		<section class="content wrap post" id="article-<?php echo article_id(); ?>">
			<h2><?php echo article_title(); ?> </h2>
			<strong><?php echo article_date(); ?></strong><br><br>
			<?php $image = article_custom_field('post_image','');
			if($image== NULL){  } else {  ?>
					<img src="<?php echo article_custom_field('post_image',''); ?>" alt="<?php echo article_title(); ?>" />
			<?php } ?>

			<article>
				<?php echo article_markdown(); ?>
			</article>

			<section class="footnote">
				<p>This article is my <?php echo numeral(total_articles()); ?> oldest. It is <?php echo count_words(article_markdown()); ?> words long<?php if(comments_open()): ?>, and it’s got <?php echo total_comments() . pluralise(total_comments(), ' comment'); ?> for now.<?php endif; ?> <?php echo article_custom_field('attribution'); ?></p>
				<p>Share:</p>
        		<a target="_blank" href="https://twitter.com/intent/tweet?text=<?php echo article_title(); ?> <?php echo getUrl(); ?> by @<?php echo twitter_account(); ?>">
        			<i class="fa fa-twitter icon"></i>
        		</a>
        		<a target="_blank" href="https://plus.google.com/share?url=<?php echo getUrl(); ?>">
        			<i class="fa fa-google-plus icon"></i>
        		</a>
				<?php if($image== NULL){  } else {  ?>
					<a target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php echo getUrl(); ?>&media=<?php echo article_custom_field('post_image',''); ?>&description=<?php echo strip_tags(substr(article_markdown(), 0, 400)); ?>">
				    	<i class="fa fa-pinterest icon"></i>
					</a>
				<?php } ?>
			</section>
		</section>
		
		<form id="comment" class="commentform wrap" method="post" action="<?php echo comment_form_url(); ?>#comment">
			<?php echo comment_form_notifications(); ?>

			<p class="name">
				<label for="name">Your name:</label>
				<?php echo comment_form_input_name('placeholder="Your name"'); ?>
			</p>

			<p class="email">
				<label for="email">Your email address:</label>
				<?php echo comment_form_input_email('placeholder="Your email (won’t be published)"'); ?>
			</p>

			<p class="textarea">
				<label for="text">Your comment:</label>
				<?php echo comment_form_input_text('placeholder="Your comment"'); ?>
			</p>

			<p class="submit">
				<?php echo comment_form_button(); ?>
			</p>
		</form>

		<?php if(comments_open()): ?>
		<section class="comments">
			<?php if(has_comments()): ?>
			<ul class="commentlist">
				<?php $i = 0; while(comments()): $i++; ?>
				<li class="comment" id="comment-<?php echo comment_id(); ?>">
					<div class="wrap">
						<h4><?php echo comment_name(); ?></h4>
						<time><?php echo relative_time(comment_time()); ?></time>

						<div class="content">
							<?php echo comment_text(); ?>
						</div>

						<span class="counter"><?php echo $i; ?></span>
					</div>
				</li>
				<?php endwhile; ?>
			</ul>
			<?php endif; ?>


		</section>
		<?php endif; ?>

<?php theme_include('footer'); ?>